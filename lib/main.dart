import 'package:flutter/material.dart';
import 'package:jiracorn/src/bloc/authentication_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/resource/session.dart';
import 'package:jiracorn/src/ui/home/home_page.dart';
import 'package:jiracorn/src/ui/login/login_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool isLoggedIn = false;

  @override
  void initState() {
    super.initState();
    autoLogIn();
  }

  void autoLogIn() async {
    final bool loggedIn = await Session.instance.isAutologinAvailable();

    if (loggedIn) {
      setState(() {
        isLoggedIn = true;
      });
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthenticationBloc>(
      bloc: AuthenticationBloc(),
      child: MaterialApp(
        title: 'Jiracorn',
        theme: ThemeData(
          primarySwatch: Colors.pink,
        ),
        home: isLoggedIn ? HomePage() : LoginPage(),
      ),
    );
  }
}
