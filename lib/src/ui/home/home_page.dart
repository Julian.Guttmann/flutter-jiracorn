import 'package:flutter/material.dart';
import 'package:jiracorn/src/bloc/issue_bloc.dart';
import 'package:jiracorn/src/bloc/profile_bloc.dart';
import 'package:jiracorn/src/bloc/project_bloc.dart';
import 'package:jiracorn/src/bloc/server_info_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/ui/issue/search_page.dart';
import 'package:jiracorn/src/ui/project/project_list_page.dart';
import 'package:jiracorn/src/ui/user/user_detail_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentPageIndex = 0;

  Widget currentPage;
  final PageStorageBucket bucket = PageStorageBucket();

  void _incrementTab(index) {
    setState(
      () {
        _currentPageIndex = index;
        currentPage = _buildPage(context, index);
      },
    );
  }

  @override
  void initState() {
    currentPage = _buildPage(context, 0);
    super.initState();
  }

  Widget _buildPage(BuildContext context, int index) {
    switch (index) {
      case 0:
        return BlocProvider<ProjectBloc>(
          bloc: ProjectBloc(),
          child: ProjectListPage(),
        );
        break;

      case 1:
        return BlocProvider<IssueBloc>(
          bloc: IssueBloc(),
          child: SearchPage(),
        );
        break;
      case 2:
        return BlocProvider<ProfileBloc>(
          bloc: ProfileBloc(),
          child: BlocProvider<ServerInfoBloc>(
            bloc: ServerInfoBloc(),
            child: UserDetailPage(),
          ),
        );
        break;
      default:
        return CircularProgressIndicator();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentPageIndex,
        type: BottomNavigationBarType.shifting,
        items: [
          _buildBottomNavigationBarItem(
            Icons.extension,
            'Projects',
          ),
          _buildBottomNavigationBarItem(
            Icons.search,
            'Issues',
          ),
          _buildBottomNavigationBarItem(
            Icons.person,
            'Profile',
          ),
        ],
        onTap: (index) {
          _incrementTab(index);
        },
      ),
      body: PageStorage(
        bucket: bucket,
        child: currentPage,
      ),
    );
  }

  BottomNavigationBarItem _buildBottomNavigationBarItem(
    IconData icon,
    String title,
  ) {
    return BottomNavigationBarItem(
      icon: Icon(icon, color: Theme.of(context).accentColor),
      title: Text(
        title,
        style: TextStyle(color: Theme.of(context).accentColor),
      ),
    );
  }
}
