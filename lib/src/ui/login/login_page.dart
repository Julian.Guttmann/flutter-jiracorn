import 'package:flutter/material.dart';
import 'package:jiracorn/src/bloc/authentication_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/ui/home/home_page.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Material(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 64),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('JIRA', style: Theme.of(context).textTheme.display4),
                SizedBox(height: 24),
                _urlField(context),
                SizedBox(height: 12),
                _usernameField(context),
                SizedBox(height: 12),
                _passwordField(context),
                SizedBox(height: 24),
                _submitButton(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _urlField(BuildContext context) {
    final authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);

    return StreamBuilder(
      stream: authenticationBloc.jiraUrl,
      builder: (context, snapshot) {
        return TextField(
          decoration: InputDecoration(
            hintText: 'Jira URL',
            fillColor: Colors.black12,
          ),
          onChanged: authenticationBloc.setJiraUrl,
          keyboardType: TextInputType.url,
        );
      },
    );
  }

  Widget _usernameField(BuildContext context) {
    final authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);

    return StreamBuilder(
      stream: authenticationBloc.username,
      builder: (context, snapshot) {
        return TextField(
          decoration: InputDecoration(
            hintText: 'Username',
            fillColor: Colors.black12,
          ),
          onChanged: authenticationBloc.setUsername,
          keyboardType: TextInputType.text,
        );
      },
    );
  }

  Widget _passwordField(BuildContext context) {
    final authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);

    return StreamBuilder(
      stream: authenticationBloc.password,
      builder: (context, snapshot) {
        return TextField(
          decoration: InputDecoration(
            hintText: 'Password',
            fillColor: Colors.black12,
          ),
          onChanged: authenticationBloc.setPassword,
          obscureText: true,
        );
      },
    );
  }

  Widget _submitButton(BuildContext context) {
    final authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    return StreamBuilder(
      stream: authenticationBloc.submitValid,
      builder: (context, snapshot) {
        return FlatButton(
          child: Text('Login'),
          color: Colors.yellow,
          textColor: Colors.white,
          onPressed: () => _loginAndNavigate(context, snapshot.hasData),
        );
      },
    );
  }

  _loginAndNavigate(BuildContext context, bool formFilled) async {
    final authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);

    if (!formFilled) {
      return;
    }
    var loginSucess = await authenticationBloc
        .submit()
        .catchError((error) => Future.value(false));

    if (loginSucess) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) {
            return HomePage();
          },
        ),
      );
    } else {
      final snackBar = SnackBar(
        content: Text('Invalid information. Could not login.'),
        action: SnackBarAction(
          label: 'Hide',
          onPressed: () {
            // Some code to undo the change.
          },
        ),
      );
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }
}
