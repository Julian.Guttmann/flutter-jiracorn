import 'package:flutter/material.dart';
import 'package:jiracorn/main.dart';
import 'package:jiracorn/src/bloc/profile_bloc.dart';
import 'package:jiracorn/src/bloc/server_info_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/model/server/server_info.dart';
import 'package:jiracorn/src/model/user/user.dart';
import 'package:jiracorn/src/resource/session.dart';

class UserDetailPage extends StatelessWidget {
  final _cardBorderRadius = 16.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: _buildBody(context),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {},
        child: IconButton(
          icon: Icon(Icons.exit_to_app),
          tooltip: 'Logout',
          onPressed: () async {
            await _asyncConfirmLogoutDialog(context);
          },
        ),
      ),
    );
  }

  Widget _buildWidgetFromSnaphot(AsyncSnapshot snapshot, Function buildWidget) {
    if (snapshot.hasData) {
      return buildWidget(snapshot.data);
    } else if (snapshot.hasError) {
      return Text(
        snapshot.error.toString(),
      );
    }
    return Center(
      child: Container(),
    );
  }

  Widget _buildBody(BuildContext context) {
    final ProfileBloc profileBloc = BlocProvider.of<ProfileBloc>(context);
    final ServerInfoBloc serverInfoBloc =
        BlocProvider.of<ServerInfoBloc>(context);
    profileBloc.fetchUser();
    serverInfoBloc.fetchServerInfo();

    return ListView(
      children: <Widget>[
        StreamBuilder<User>(
            stream: profileBloc.user,
            builder: (context, snapshot) {
              return _buildWidgetFromSnaphot(snapshot, _buildTitleCard);
            }),
        StreamBuilder<User>(
            stream: profileBloc.user,
            builder: (context, snapshot) {
              return _buildWidgetFromSnaphot(snapshot, _buildDetailsCard);
            }),
        StreamBuilder<ServerInfo>(
            stream: serverInfoBloc.serverInfo,
            builder: (context, snapshot) {
              return _buildWidgetFromSnaphot(snapshot, _buildServerInfoCard);
            }),
      ],
    );
  }

  Widget _buildTitleCard(User user) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_cardBorderRadius),
      ),
      child: Column(
        children: [
          ListTile(
            title: Text(
              user.displayName,
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            /* leading: Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: NetworkImageUtils.loadImage(user.avatarUrls.s48x48),
                ),
              ),
            ), */
          ),
        ],
      ),
    );
  }

  Widget _buildDetailsCard(User user) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_cardBorderRadius),
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text("Details"),
          ),
          Divider(),
          ListTile(
            leading: Text("Username:"),
            trailing: Text(user.name),
          ),
          ListTile(
            leading: Text("E-Mail:"),
            trailing: Text(user.emailAddress),
          ),
        ],
      ),
    );
  }

  Widget _buildServerInfoCard(ServerInfo serverInfo) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_cardBorderRadius),
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text("Serverinfo"),
          ),
          Divider(),
          ListTile(
            leading: Text("Base Url:"),
            trailing: Text(serverInfo.baseUrl),
          ),
          ListTile(
            leading: Text("Version:"),
            trailing: Text(serverInfo.version),
          ),
          ListTile(
            leading: Text("Server title:"),
            trailing: Text(serverInfo.serverTitle),
          ),
        ],
      ),
    );
  }
}

enum ConfirmAction { CANCEL, ACCEPT }

Future<ConfirmAction> _asyncConfirmLogoutDialog(BuildContext context) async {
  return showDialog<ConfirmAction>(
    context: context,
    barrierDismissible: false, // user must tap button for close dialog!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Warning'),
        content: const Text('Do you really want to log out?'),
        actions: <Widget>[
          FlatButton(
            child: const Text('CANCEL'),
            onPressed: () => Navigator.of(context).pop(ConfirmAction.CANCEL),
          ),
          FlatButton(
            child: const Text('ACCEPT'),
            onPressed: () {
              // Navigate to the login screen and remove past routes.
              // This disables using back-button to reverse to this view
              logout(context);
            },
          )
        ],
      );
    },
  );
}

logout(BuildContext context) async {
  await Session.instance.logout();
  Navigator.of(context).pushAndRemoveUntil(
    MaterialPageRoute(
      builder: (context) {
        return MyApp();
      },
    ),
    // In this situation it removes all of the routes except for the new route I pushed.
    (Route<dynamic> route) => false,
  );
}
