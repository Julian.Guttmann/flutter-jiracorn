import 'package:flutter/material.dart';
import 'package:jiracorn/src/bloc/issue_edit_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/model/issue/issue.dart';

class IssueEditPage extends StatelessWidget {
  final _cardBorderRadius = 16.0;
  final Issue _issue;

  const IssueEditPage(this._issue);

  @override
  Widget build(BuildContext context) {
    final issueEditBloc = BlocProvider.of<IssueEditBloc>(context);

    return Scaffold(
      body: Container(
        child: _buildCards(issueEditBloc),
      ),
      floatingActionButton: Builder(
        builder: (BuildContext context) {
          return FloatingActionButton(
            onPressed: () async {
              final snackBar = SnackBar(
                content: Text('Could not save issue'),
                action: SnackBarAction(
                  label: 'Hide',
                  onPressed: () {
                    // Some code to undo the change.
                  },
                ),
              );
              var a =
                  await issueEditBloc.updateIssue(_issue).catchError((onError) {
                // Find the Scaffold in the widget tree and use
                // it to show a SnackBar.
                Scaffold.of(context).showSnackBar(snackBar);
              });
              if (a != null && a == true) {
                Navigator.pop(context);
              }
            },
            child: Icon(Icons.save),
          );
        },
      ),
    );
  }

  Widget _buildCards(IssueEditBloc issueEditBloc) {
    return ListView(
      children: <Widget>[
        _buildTitleEditCard(issueEditBloc),
        _buildDescriptionEditCard(issueEditBloc),
      ],
    );
  }

  Widget _buildTitleEditCard(IssueEditBloc issueEditBloc) {
    final TextEditingController textEditingController = TextEditingController(
      text: _issue.fields.summary,
    );

    return Card(
      child: TextField(
        controller: textEditingController,
        onChanged: (value) => _issue.fields.summary = value,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Title",
        ),
      ),
    );
  }

  Widget _buildDescriptionEditCard(IssueEditBloc issueEditBloc) {
    final TextEditingController textEditingController = TextEditingController(
      text: _issue.fields.description,
    );

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_cardBorderRadius),
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text("Description:"),
          ),
          Divider(),
          Container(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                controller: textEditingController,
                keyboardType: TextInputType.multiline,
                onChanged: (value) => _issue.fields.description = value,
                maxLines: null,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
