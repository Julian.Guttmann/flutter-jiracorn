import 'package:flutter/material.dart';
import 'package:jiracorn/src/bloc/issue_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/model/issue/issue.dart';
import 'package:jiracorn/src/widgets/issue/issue_list_by_project_widget.dart';

class IssueSearchPage extends StatelessWidget {
  final _projectId;
  IssueSearchPage(this._projectId);

  @override
  Widget build(BuildContext context) {
    final IssueBloc issueBloc = BlocProvider.of<IssueBloc>(context);
    issueBloc.fetchAllIssueByProjectId(_projectId);

    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  onChanged: (String text) {
                    issueBloc.searchIssueInProject(text, _projectId);
                  },
                  decoration: InputDecoration(
                    labelText: "Search in $_projectId",
                    hintText: "Search",
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20.0),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: StreamBuilder<List<Issue>>(
                  stream: issueBloc.allIssueByProjectId,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return IssueListByProject(
                        snapshot.data,
                        issueBloc.fetchAllIssueByProjectId,
                        _projectId,
                      );
                    } else if (snapshot.hasError) {
                      return Text(
                        snapshot.error.toString(),
                      );
                    }
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
