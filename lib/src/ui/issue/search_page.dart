import 'package:flutter/material.dart';
import 'package:jiracorn/src/bloc/issue_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/model/issue/issue.dart';
import 'package:jiracorn/src/widgets/issue/issue_list_widget.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    IssueBloc issueBloc = BlocProvider.of<IssueBloc>(context);
    issueBloc.fetchAllLastViewedIssue();

    return SafeArea(
      child: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (String text) {
                  issueBloc.searchIssue(text);
                },
                decoration: InputDecoration(
                  labelText: "Search",
                  hintText: "Search",
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: StreamBuilder<List<Issue>>(
                stream: issueBloc.allLastViewedIssue,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return IssueList(
                        snapshot.data, issueBloc.fetchAllLastViewedIssue);
                  } else if (snapshot.hasError) {
                    return Text(
                      snapshot.error.toString(),
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
