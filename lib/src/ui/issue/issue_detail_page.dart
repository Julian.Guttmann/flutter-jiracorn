import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:jiracorn/src/bloc/issue_detail_bloc.dart';
import 'package:jiracorn/src/bloc/issue_edit_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/model/issue/issue.dart';

import 'issue_edit_page.dart';

class IssueDetailPage extends StatelessWidget {
  final _cardBorderRadius = 16.0;
  final String _issueKey;

  const IssueDetailPage(this._issueKey);

  @override
  Widget build(BuildContext context) {
    final IssueDetailBloc issueDetailBloc =
        BlocProvider.of<IssueDetailBloc>(context);

    issueDetailBloc.fetchCurrentIssue(_issueKey);

    return StreamBuilder<Issue>(
      stream: issueDetailBloc.currentIssue,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return _buildIssueDetailPage(snapshot.data, context);
        } else if (snapshot.hasError) {
          return Text(
            snapshot.error.toString(),
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  Widget _buildIssueDetailPage(Issue issue, BuildContext context) {
    final IssueDetailBloc issueDetailBloc =
        BlocProvider.of<IssueDetailBloc>(context);

    return Scaffold(
      body: Container(
        child: _buildCards(issue),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context)
            .push(
              MaterialPageRoute(
                builder: (context) => BlocProvider<IssueEditBloc>(
                  bloc: IssueEditBloc(),
                  child: IssueEditPage(issue),
                ),
              ),
            )
            .then((value) => value != null && value == true
                ? issueDetailBloc.fetchCurrentIssue(_issueKey)
                : null),
        child: Icon(Icons.edit),
      ),
    );
  }

  Widget _buildCards(Issue issue) {
    return ListView(
      children: <Widget>[
        _buildTitleCard(issue),
        _buildStatusCard(issue),
        _buildDescriptionCard(issue),
      ],
    );
  }

  Widget _buildTitleCard(Issue issue) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_cardBorderRadius),
      ),
      child: Column(
        children: [
          ListTile(
            title: Text(
              '${issue.key}: ${issue.fields.summary}',
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            subtitle: Text(
              '${issue.fields.project.key}',
              style: TextStyle(
                color: Colors.lightBlue,
              ),
            ),
            /*
            leading: Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image(
                  image: AssetImage('assets/${random.nextInt(10)}.png'),
                ),
              ),
            ),
            */
          ),
        ],
      ),
    );
  }

  Widget _buildStatusCard(Issue issue) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_cardBorderRadius),
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text("Status:"),
            trailing: Chip(
              backgroundColor: Colors.grey.shade100,
              label: Text('${issue.fields.status.name}'),
            ),
          ),
          ListTile(
            title: Text("Type:"),
            trailing: Chip(
              backgroundColor: Colors.grey.shade100,
              label: Text('${issue.fields.issuetype.name}'),
            ),
          ),
          ListTile(
            title: Text("Priority:"),
            trailing: Chip(
              backgroundColor: Colors.grey.shade100,
              label: Text('${issue.fields.priority.name}'),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDescriptionCard(Issue issue) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_cardBorderRadius),
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text("Description:"),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              issue.fields.description != null ? issue.fields.description : '',
              textAlign: TextAlign.justify,
            ),
          ),
        ],
      ),
    );
  }
}
