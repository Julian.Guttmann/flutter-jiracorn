import 'package:flutter/material.dart';
import 'package:jiracorn/src/bloc/project_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/model/project/project.dart';
import 'package:jiracorn/src/ui/project/project_list_element.dart';

class ProjectListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return buildStreamBuilder(context);
  }

  StreamBuilder<List<Project>> buildStreamBuilder(
    BuildContext context,
  ) {
    final ProjectBloc projectBloc = BlocProvider.of<ProjectBloc>(context);
    projectBloc.fetchAllProjectListItem();

    return StreamBuilder(
      stream: projectBloc.allProjectListItem,
      builder: (context, AsyncSnapshot<List<Project>> snapshot) {
        if (snapshot.hasData) {
          return buildList(snapshot);
        } else if (snapshot.hasError) {
          return Text(
            snapshot.error.toString(),
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  Widget buildList(AsyncSnapshot<List<Project>> snapshot) {
    return ListView.builder(
      itemCount: snapshot.data.length,
      itemBuilder: (BuildContext context, int index) {
        return ProjectListElement(snapshot.data[index]);
      },
    );
  }
}
