import 'package:flutter/material.dart';
import 'package:jiracorn/src/bloc/issue_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/model/project/project.dart';
import 'package:jiracorn/src/ui/issue/issue_search_page.dart';

class ProjectListElement extends StatelessWidget {
  const ProjectListElement(this._itemModel);
  final Project _itemModel;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => BlocProvider<IssueBloc>(
                bloc: IssueBloc(),
                child: IssueSearchPage(_itemModel.key),
              ),
            ),
          ),
          title: Text(_itemModel.name),
          subtitle: Text(_itemModel.key),
          trailing: Icon(Icons.keyboard_arrow_right),
        ),
        Divider(),
      ],
    );
  }
}
