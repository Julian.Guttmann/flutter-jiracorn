import 'dart:convert';

import 'package:http/http.dart';
import 'package:jiracorn/src/model/issue/all_issue.dart';
import 'package:jiracorn/src/model/issue/issue.dart';
import 'package:jiracorn/src/model/issue/update_field_model.dart';
import 'package:jiracorn/src/resource/session.dart';

class IssueApiProvider {
  Session session = Session.instance;

  static String _apiPathSearch = '/rest/api/2/search';
  static String _apiPathIssue = '/rest/api/2/issue';

  static String _jqlSearchPath = '?jql=';

  static String _jqlLastViewedIssues =
      'issuekey in issueHistory() ORDER BY lastViewed DESC';

  Future<AllIssue> fetchAllLastViewedIssue(int startAtIndex) async {
    final Response response = await session.get(_apiPathSearch +
        _jqlSearchPath +
        _jqlLastViewedIssues +
        '&startAt=$startAtIndex');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return AllIssue.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load last viewed issues');
    }
  }

  Future<AllIssue> fetchAllIssueByProjectID(
      String projectId, int startAtIndex) async {
    final Response response = await session.get(_apiPathSearch +
        _jqlSearchPath +
        'project=$projectId' +
        '&startAt=$startAtIndex');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return AllIssue.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load issues for project $projectId');
    }
  }

  Future<AllIssue> fetchAllSearchIssueInProject(
      String searchIssue, String projectId) async {
    final Response response = await session.get(_apiPathSearch +
        _jqlSearchPath +
        'project=$projectId' +
        ' AND summary ~ "$searchIssue"');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return AllIssue.fromJson(json.decode(response.body));
    } else {
      throw Exception(
          'Failed to search issues for project $projectId with: $searchIssue');
    }
  }

  Future<AllIssue> fetchAllSeachIssue(String searchIssue) async {
    final Response response = await session
        .get(_apiPathSearch + _jqlSearchPath + 'summary ~ "$searchIssue"');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return AllIssue.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to search for issues with: $searchIssue');
    }
  }

  Future<Issue> fetchIssueByKey(String issueKey) async {
    final Response response = await session.get('$_apiPathIssue/$issueKey');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return Issue.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to get issue with key: $issueKey');
    }
  }

  Future<bool> updateIssue(Issue issue) async {
    final updateBody = UpdateFieldModel();
    updateBody.fields = Fields();
    updateBody.fields.summary = issue.fields.summary;
    updateBody.fields.description = issue.fields.description;

    //call api and save issue oda so

    final Response response =
        await session.put('$_apiPathIssue/${issue.key}', updateBody.toJson());
    if (response.statusCode == 204) {
      return true;
    } else {
      throw Exception('Failed to update issue ${issue.key} with: $updateBody');
    }
  }

  List<Issue> parseAllIssue(String responseBody) {
    final parsed = AllIssue.fromJson(json.decode(responseBody));
    return parsed.issues;
  }
}
