import 'dart:convert';

import 'package:jiracorn/src/model/project/project.dart';
import 'package:jiracorn/src/resource/session.dart';

class ProjectApiProvider {
  Session session = Session.instance;

  final String _apiPath = '/rest/api/2/project';

  List<Project> parseAllProjectListItem(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Project>((json) => Project.fromJson(json)).toList();
  }

  Future<List<Project>> fetchAllProjectListItem() async {
    final response = await session.get(_apiPath);

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return parseAllProjectListItem(response.body);
    } else {
      throw Exception('Failed to load list of Projects');
    }
  }
}
