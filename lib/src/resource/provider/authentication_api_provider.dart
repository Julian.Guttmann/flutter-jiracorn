import 'package:jiracorn/src/resource/session.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenticationApiProvider {
  Session session = Session.instance;

  final String _apiPath = '/rest/auth/1/session';

  Future<bool> authenticate(
      String jiraUrl, String username, String password) async {
    session.setBasicAuthHeader(username, password);
    session.baseUrl = jiraUrl;

    final response = await session.get(_apiPath);

    final status = response.statusCode;

    if (status == 200) {
      _addOrUpdateSession(jiraUrl, username, password);
      return true;
    }
    return false;
  }

  void _addOrUpdateSession(
      String jiraUrl, String username, String password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setString('jiraUrl', jiraUrl);
    prefs.setString('username', username);
    prefs.setString('password', password);
  }
}
