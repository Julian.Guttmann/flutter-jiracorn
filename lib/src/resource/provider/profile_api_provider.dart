import 'dart:convert';

import 'package:http/http.dart';
import 'package:jiracorn/src/model/user/user.dart';
import 'package:jiracorn/src/resource/session.dart';

class ProfileApiProvider {
  Session session = Session.instance;

  final String _apiPath = '/rest/api/2/myself';

  Future<User> fetchUser() async {
    final Response response = await session.get(_apiPath);

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return User.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load user');
    }
  }
}
