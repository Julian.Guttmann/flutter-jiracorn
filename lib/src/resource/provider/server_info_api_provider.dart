import 'dart:convert';

import 'package:http/http.dart';
import 'package:jiracorn/src/model/server/server_info.dart';
import 'package:jiracorn/src/resource/session.dart';

class ServerInfoApiProvider {
  Session session = Session.instance;

  final String _apiPath = '/rest/api/2/serverInfo';

  Future<ServerInfo> fetchServerInfo() async {
    final Response response = await session.get(_apiPath);

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return ServerInfo.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load server info');
    }
  }
}
