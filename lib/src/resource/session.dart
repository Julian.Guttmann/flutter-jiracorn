import 'dart:convert';

import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Session {
  Client http = Client();

  factory Session() => _getInstance();
  static Session get instance => _getInstance();
  static Session _instance;

  // Private constructor
  Session._internal() {
    //init
  }

  static Session _getInstance() {
    if (_instance == null) {
      _instance = new Session._internal();
    }
    return _instance;
  }

  String _baseUrl;

  set baseUrl(String baseUrl) => _baseUrl = baseUrl;

  Map<String, String> headers = {
    'Content-Type': 'application/json',
  };
  Future<Response> getFromUrl(String url) async {
    await _initUserSession();
    Response response = await http.get(url, headers: headers);
    return response;
  }

  Future<Response> get(String urlSuffix) async {
    await _initUserSession();
    Response response = await http.get(_baseUrl + urlSuffix, headers: headers);
    return response;
  }

  Future<Map> post(String urlSuffix, dynamic data) async {
    await _initUserSession();

    Response response =
        await http.post(_baseUrl + urlSuffix, body: data, headers: headers);
    return json.decode(response.body);
  }

  Future<Response> put(String urlSuffix, Map<String, dynamic> data) async {
    await _initUserSession();

    Response response = await http.put(_baseUrl + urlSuffix,
        body: json.encode(data), headers: headers);
    return response;
  }

  void setBasicAuthHeader(final String username, final String password) {
    String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$username:$password'));
    headers.putIfAbsent('authorization', () => basicAuth);
  }

  Future _initUserSession() async {
    if (await isAutologinAvailable()) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var username = prefs.getString('username');
      var password = prefs.getString('password');
      _baseUrl = prefs.getString('jiraUrl');
      setBasicAuthHeader(username, password);
    }
  }

  isAutologinAvailable() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (prefs.getString('username') != null &&
        prefs.getString('password') != null &&
        prefs.getString('jiraUrl') != null) {
      return true;
    }
    return false;
  }

  logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('username');
    prefs.remove('password');
    prefs.remove('jiraUrl');
    _baseUrl = "";
  }
}