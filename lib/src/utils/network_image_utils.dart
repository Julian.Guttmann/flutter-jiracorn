import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:jiracorn/src/resource/session.dart';

class NetworkImageUtils {
  static Widget loadImage(String url) {
    Session session = Session();
    return FutureBuilder(
      future: session.getFromUrl(url),
      builder: (BuildContext context, AsyncSnapshot<Response> response) {
        if (response.hasData) {
          bool isSVG = false;
          try {
            isSVG = response.data.body.toString().contains('SVG');
          } catch (e) {}
          return isSVG
              ? Image.asset('assets/no_image.jpg')
              : Image.memory(response.data.bodyBytes, fit: BoxFit.fill);
        } else {
          return Container(); // placeholder
        }
      },
    );
  }
}
