import 'package:jiracorn/src/model/project/avatar_urls.dart';

class User {
  String self;
  String key;
  String name;
  String emailAddress;
  AvatarUrls avatarUrls;
  String displayName;
  bool active;
  String timeZone;
  String locale;
  String expand;

  User({
    this.self,
    this.key,
    this.name,
    this.emailAddress,
    this.avatarUrls,
    this.displayName,
    this.active,
    this.timeZone,
    this.locale,
    this.expand,
  });

  User.fromJson(Map<String, dynamic> json) {
    self = json['self'];
    key = json['key'];
    name = json['name'];
    emailAddress = json['emailAddress'];
    avatarUrls = json['avatarUrls'] != null
        ? new AvatarUrls.fromJson(json['avatarUrls'])
        : null;
    displayName = json['displayName'];
    active = json['active'];
    timeZone = json['timeZone'];
    locale = json['locale'];
    expand = json['expand'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['self'] = this.self;
    data['key'] = this.key;
    data['name'] = this.name;
    data['emailAddress'] = this.emailAddress;
    if (this.avatarUrls != null) {
      data['avatarUrls'] = this.avatarUrls.toJson();
    }
    data['displayName'] = this.displayName;
    data['active'] = this.active;
    data['timeZone'] = this.timeZone;
    data['locale'] = this.locale;
    data['expand'] = this.expand;
    return data;
  }
}
