import 'package:jiracorn/src/model/issue/assignee.dart';
import 'package:jiracorn/src/model/issue/comment.dart';
import 'package:jiracorn/src/model/issue/creator.dart';
import 'package:jiracorn/src/model/issue/fix_versions.dart';
import 'package:jiracorn/src/model/issue/issue_links.dart';
import 'package:jiracorn/src/model/issue/issue_type.dart';
import 'package:jiracorn/src/model/issue/priority.dart';
import 'package:jiracorn/src/model/issue/progress.dart';
import 'package:jiracorn/src/model/issue/reporter.dart';
import 'package:jiracorn/src/model/issue/status.dart';
import 'package:jiracorn/src/model/issue/subtasks.dart';
import 'package:jiracorn/src/model/project/project.dart';

class Fields {
  Issuetype issuetype;
  int timespent;
  Project project;
  List<FixVersions> fixVersions;
  String lastViewed;
  String created;
  Priority priority;
  List<Issuelinks> issuelinks;
  Assignee assignee;
  String updated;
  Status status;
  String description;
  String summary;
  Creator creator;
  List<Subtasks> subtasks;

  Reporter reporter;

  Progress progress;
  Comment comment;

  Fields({
    this.issuetype,
    this.timespent,
    this.project,
    this.fixVersions,
    this.lastViewed,
    this.created,
    this.priority,
    this.assignee,
    this.updated,
    this.status,
    this.description,
    this.summary,
    this.creator,
    this.subtasks,
    this.reporter,
    this.progress,
    this.comment,
  });

  Fields.fromJson(Map<String, dynamic> json) {
    issuetype = json['issuetype'] != null
        ? new Issuetype.fromJson(json['issuetype'])
        : null;
    timespent = json['timespent'];
    project =
        json['project'] != null ? new Project.fromJson(json['project']) : null;
    if (json['fixVersions'] != null) {
      fixVersions = new List<FixVersions>();
      json['fixVersions'].forEach((v) {
        fixVersions.add(new FixVersions.fromJson(v));
      });
    }
    created = json['created'];

    priority = json['priority'] != null
        ? new Priority.fromJson(json['priority'])
        : null;
    if (json['issuelinks'] != null) {
      issuelinks = new List<Issuelinks>();
      json['issuelinks'].forEach((v) {
        issuelinks.add(new Issuelinks.fromJson(v));
      });
    }
    assignee = json['assignee'] != null
        ? new Assignee.fromJson(json['assignee'])
        : null;
    updated = json['updated'];
    status =
        json['status'] != null ? new Status.fromJson(json['status']) : null;
    description = json['description'];
    summary = json['summary'];
    creator =
        json['creator'] != null ? new Creator.fromJson(json['creator']) : null;
    if (json['subtasks'] != null) {
      subtasks = new List<Subtasks>();
      json['subtasks'].forEach((v) {
        subtasks.add(new Subtasks.fromJson(v));
      });
    }
    reporter = json['reporter'] != null
        ? new Reporter.fromJson(json['reporter'])
        : null;
    progress = json['progress'] != null
        ? new Progress.fromJson(json['progress'])
        : null;
    comment =
        json['comment'] != null ? new Comment.fromJson(json['comment']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.issuetype != null) {
      data['issuetype'] = this.issuetype.toJson();
    }
    data['timespent'] = this.timespent;
    if (this.project != null) {
      data['project'] = this.project.toJson();
    }
    if (this.fixVersions != null) {
      data['fixVersions'] = this.fixVersions.map((v) => v.toJson()).toList();
    }

    data['lastViewed'] = this.lastViewed;

    data['created'] = this.created;
    if (this.priority != null) {
      data['priority'] = this.priority.toJson();
    }
    if (this.issuelinks != null) {
      data['issuelinks'] = this.issuelinks.map((v) => v.toJson()).toList();
    }
    if (this.assignee != null) {
      data['assignee'] = this.assignee.toJson();
    }
    data['updated'] = this.updated;
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    data['description'] = this.description;
    data['summary'] = this.summary;
    if (this.creator != null) {
      data['creator'] = this.creator.toJson();
    }
    if (this.subtasks != null) {
      data['subtasks'] = this.subtasks.map((v) => v.toJson()).toList();
    }
    if (this.reporter != null) {
      data['reporter'] = this.reporter.toJson();
    }
    if (this.progress != null) {
      data['progress'] = this.progress.toJson();
    }
    if (this.comment != null) {
      data['comment'] = this.comment.toJson();
    }
    return data;
  }
}
