class UpdateFieldModel {
  Fields fields;

  UpdateFieldModel({this.fields});

  UpdateFieldModel.fromJson(Map<String, dynamic> json) {
    fields =
        json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.fields != null) {
      data['fields'] = this.fields.toJson();
    }
    return data;
  }
}

class Fields {
  String description;
  String summary;

  Fields({this.description, this.summary});

  Fields.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    summary = json['summary'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['summary'] = this.summary;
    return data;
  }
}
