import 'package:jiracorn/src/model/issue/comments.dart';

class Comment {
  List<Comments> comments;
  int maxResults;
  int total;
  int startAt;

  Comment({this.comments, this.maxResults, this.total, this.startAt});

  Comment.fromJson(Map<String, dynamic> json) {
    if (json['comments'] != null) {
      comments = new List<Comments>();
      json['comments'].forEach((v) {
        comments.add(new Comments.fromJson(v));
      });
    }
    maxResults = json['maxResults'];
    total = json['total'];
    startAt = json['startAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    data['maxResults'] = this.maxResults;
    data['total'] = this.total;
    data['startAt'] = this.startAt;
    return data;
  }
}
