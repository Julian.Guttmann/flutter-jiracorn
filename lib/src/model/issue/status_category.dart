class StatusCategory {
  String self;
  int id;
  String key;
  String colorName;
  String name;

  StatusCategory({this.self, this.id, this.key, this.colorName, this.name});

  StatusCategory.fromJson(Map<String, dynamic> json) {
    self = json['self'];
    id = json['id'];
    key = json['key'];
    colorName = json['colorName'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['self'] = this.self;
    data['id'] = this.id;
    data['key'] = this.key;
    data['colorName'] = this.colorName;
    data['name'] = this.name;
    return data;
  }
}
