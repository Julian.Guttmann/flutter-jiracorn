class Progress {
  int progress;
  int total;
  int percent;

  Progress({this.progress, this.total, this.percent});

  Progress.fromJson(Map<String, dynamic> json) {
    progress = json['progress'];
    total = json['total'];
    percent = json['percent'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['progress'] = this.progress;
    data['total'] = this.total;
    data['percent'] = this.percent;
    return data;
  }
}