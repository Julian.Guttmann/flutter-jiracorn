class FixVersions {
  String self;
  String id;
  String description;
  String name;
  bool archived;
  bool released;
  String releaseDate;

  FixVersions(
      {this.self,
      this.id,
      this.description,
      this.name,
      this.archived,
      this.released,
      this.releaseDate});

  FixVersions.fromJson(Map<String, dynamic> json) {
    self = json['self'];
    id = json['id'];
    description = json['description'];
    name = json['name'];
    archived = json['archived'];
    released = json['released'];
    releaseDate = json['releaseDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['self'] = this.self;
    data['id'] = this.id;
    data['description'] = this.description;
    data['name'] = this.name;
    data['archived'] = this.archived;
    data['released'] = this.released;
    data['releaseDate'] = this.releaseDate;
    return data;
  }
}
