class Priority {
  String self;
  String iconUrl;
  String name;
  String id;

  Priority({this.self, this.iconUrl, this.name, this.id});

  Priority.fromJson(Map<String, dynamic> json) {
    self = json['self'];
    iconUrl = json['iconUrl'];
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['self'] = this.self;
    data['iconUrl'] = this.iconUrl;
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}
