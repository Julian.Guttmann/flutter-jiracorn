import 'fields.dart';

class Issue {
  String expand;
  String id;
  String self;
  String key;
  Fields fields;

  Issue({this.expand, this.id, this.self, this.key, this.fields});

  Issue.fromJson(Map<String, dynamic> json) {
    expand = json['expand'];
    id = json['id'];
    self = json['self'];
    key = json['key'];
    fields =
        json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['expand'] = this.expand;
    data['id'] = this.id;
    data['self'] = this.self;
    data['key'] = this.key;
    if (this.fields != null) {
      data['fields'] = this.fields.toJson();
    }
    return data;
  }
}