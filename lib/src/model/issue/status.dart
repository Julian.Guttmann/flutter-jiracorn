import 'package:jiracorn/src/model/issue/status_category.dart';

class Status {
  String self;
  String description;
  String iconUrl;
  String name;
  String id;
  StatusCategory statusCategory;

  Status(
      {this.self,
      this.description,
      this.iconUrl,
      this.name,
      this.id,
      this.statusCategory});

  Status.fromJson(Map<String, dynamic> json) {
    self = json['self'];
    description = json['description'];
    iconUrl = json['iconUrl'];
    name = json['name'];
    id = json['id'];
    statusCategory = json['statusCategory'] != null
        ? new StatusCategory.fromJson(json['statusCategory'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['self'] = this.self;
    data['description'] = this.description;
    data['iconUrl'] = this.iconUrl;
    data['name'] = this.name;
    data['id'] = this.id;
    if (this.statusCategory != null) {
      data['statusCategory'] = this.statusCategory.toJson();
    }
    return data;
  }
}