import 'package:jiracorn/src/model/issue/issue.dart';

class AllIssue {
  String expand;
  int startAt;
  int maxResults;
  int total;
  List<Issue> issues;

  AllIssue(
      {this.expand, this.startAt, this.maxResults, this.total, this.issues});

  AllIssue.fromJson(Map<String, dynamic> json) {
    expand = json['expand'];
    startAt = json['startAt'];
    maxResults = json['maxResults'];
    total = json['total'];
    if (json['issues'] != null) {
      issues = new List<Issue>();
      json['issues'].forEach((v) {
        issues.add(new Issue.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['expand'] = this.expand;
    data['startAt'] = this.startAt;
    data['maxResults'] = this.maxResults;
    data['total'] = this.total;
    if (this.issues != null) {
      data['issues'] = this.issues.map((v) => v.toJson()).toList();
    }
    return data;
  }
}