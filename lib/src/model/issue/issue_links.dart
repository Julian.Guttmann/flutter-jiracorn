import 'package:jiracorn/src/model/issue/outward_issue.dart';
import 'package:jiracorn/src/model/issue/type.dart';

class Issuelinks {
  String id;
  String self;
  Type type;
  OutwardIssue outwardIssue;

  Issuelinks({this.id, this.self, this.type, this.outwardIssue});

  Issuelinks.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    self = json['self'];
    type = json['type'] != null ? new Type.fromJson(json['type']) : null;
    outwardIssue = json['outwardIssue'] != null
        ? new OutwardIssue.fromJson(json['outwardIssue'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['self'] = this.self;
    if (this.type != null) {
      data['type'] = this.type.toJson();
    }
    if (this.outwardIssue != null) {
      data['outwardIssue'] = this.outwardIssue.toJson();
    }
    return data;
  }
}
