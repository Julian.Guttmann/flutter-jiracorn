import 'package:jiracorn/src/model/project/avatar_urls.dart';

class Author {
  String self;
  String name;
  String key;
  String emailAddress;
  AvatarUrls avatarUrls;
  String displayName;
  bool active;
  String timeZone;

  Author(
      {this.self,
      this.name,
      this.key,
      this.emailAddress,
      this.avatarUrls,
      this.displayName,
      this.active,
      this.timeZone});

  Author.fromJson(Map<String, dynamic> json) {
    self = json['self'];
    name = json['name'];
    key = json['key'];
    emailAddress = json['emailAddress'];
    avatarUrls = json['avatarUrls'] != null
        ? new AvatarUrls.fromJson(json['avatarUrls'])
        : null;
    displayName = json['displayName'];
    active = json['active'];
    timeZone = json['timeZone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['self'] = this.self;
    data['name'] = this.name;
    data['key'] = this.key;
    data['emailAddress'] = this.emailAddress;
    if (this.avatarUrls != null) {
      data['avatarUrls'] = this.avatarUrls.toJson();
    }
    data['displayName'] = this.displayName;
    data['active'] = this.active;
    data['timeZone'] = this.timeZone;
    return data;
  }
}