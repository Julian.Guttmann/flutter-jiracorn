class Issuetype {
  String self;
  String id;
  String description;
  String iconUrl;
  String name;
  bool subtask;
  int avatarId;

  Issuetype(
      {this.self,
      this.id,
      this.description,
      this.iconUrl,
      this.name,
      this.subtask,
      this.avatarId});

  Issuetype.fromJson(Map<String, dynamic> json) {
    self = json['self'];
    id = json['id'];
    description = json['description'];
    iconUrl = json['iconUrl'];
    name = json['name'];
    subtask = json['subtask'];
    avatarId = json['avatarId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['self'] = this.self;
    data['id'] = this.id;
    data['description'] = this.description;
    data['iconUrl'] = this.iconUrl;
    data['name'] = this.name;
    data['subtask'] = this.subtask;
    data['avatarId'] = this.avatarId;
    return data;
  }
}
