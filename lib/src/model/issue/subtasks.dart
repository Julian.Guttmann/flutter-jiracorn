import 'fields.dart';

class Subtasks {
  String id;
  String key;
  String self;
  Fields fields;

  Subtasks({this.id, this.key, this.self, this.fields});

  Subtasks.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    key = json['key'];
    self = json['self'];
    fields =
        json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['key'] = this.key;
    data['self'] = this.self;
    if (this.fields != null) {
      data['fields'] = this.fields.toJson();
    }
    return data;
  }
}
