class Type {
  String id;
  String name;
  String inward;
  String outward;
  String self;

  Type({this.id, this.name, this.inward, this.outward, this.self});

  Type.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    inward = json['inward'];
    outward = json['outward'];
    self = json['self'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['inward'] = this.inward;
    data['outward'] = this.outward;
    data['self'] = this.self;
    return data;
  }
}
