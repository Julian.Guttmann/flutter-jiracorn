import 'package:jiracorn/src/model/issue/author.dart';
import 'package:jiracorn/src/model/issue/update_author.dart';

class Comments {
  String self;
  String id;
  Author author;
  String body;
  UpdateAuthor updateAuthor;
  String created;
  String updated;

  Comments(
      {this.self,
      this.id,
      this.author,
      this.body,
      this.updateAuthor,
      this.created,
      this.updated});

  Comments.fromJson(Map<String, dynamic> json) {
    self = json['self'];
    id = json['id'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
    body = json['body'];
    updateAuthor = json['updateAuthor'] != null
        ? new UpdateAuthor.fromJson(json['updateAuthor'])
        : null;
    created = json['created'];
    updated = json['updated'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['self'] = this.self;
    data['id'] = this.id;
    if (this.author != null) {
      data['author'] = this.author.toJson();
    }
    data['body'] = this.body;
    if (this.updateAuthor != null) {
      data['updateAuthor'] = this.updateAuthor.toJson();
    }
    data['created'] = this.created;
    data['updated'] = this.updated;
    return data;
  }
}
