class AvatarUrls {
  String s48x48;
  String s24x24;
  String s16x16;
  String s32x32;

  AvatarUrls({
    this.s48x48,
    this.s24x24,
    this.s16x16,
    this.s32x32,
  });

  AvatarUrls.fromJson(Map<String, dynamic> json) {
    s48x48 = json['48x48'];
    s24x24 = json['24x24'];
    s16x16 = json['16x16'];
    s32x32 = json['32x32'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['48x48'] = this.s48x48;
    data['24x24'] = this.s24x24;
    data['16x16'] = this.s16x16;
    data['32x32'] = this.s32x32;
    return data;
  }
}
