import 'package:jiracorn/src/model/project/avatar_urls.dart';
import 'package:jiracorn/src/model/project/project_category.dart';

class Project {
  String expand;
  String self;
  String id;
  String key;
  String name;
  AvatarUrls avatarUrls;
  ProjectCategory projectCategory;
  String projectTypeKey;

  Project({
    this.expand,
    this.self,
    this.id,
    this.key,
    this.name,
    this.avatarUrls,
    this.projectCategory,
    this.projectTypeKey,
  });

  Project.fromJson(Map<String, dynamic> json) {
    expand = json['expand'];
    self = json['self'];
    id = json['id'];
    key = json['key'];
    name = json['name'];
    avatarUrls = json['avatarUrls'] != null
        ? new AvatarUrls.fromJson(json['avatarUrls'])
        : null;
    projectCategory = json['projectCategory'] != null
        ? new ProjectCategory.fromJson(json['projectCategory'])
        : null;
    projectTypeKey = json['projectTypeKey'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['expand'] = this.expand;
    data['self'] = this.self;
    data['id'] = this.id;
    data['key'] = this.key;
    data['name'] = this.name;
    if (this.avatarUrls != null) {
      data['avatarUrls'] = this.avatarUrls.toJson();
    }
    if (this.projectCategory != null) {
      data['projectCategory'] = this.projectCategory.toJson();
    }
    data['projectTypeKey'] = this.projectTypeKey;
    return data;
  }
}
