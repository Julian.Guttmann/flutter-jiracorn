class ProjectCategory {
  String self;
  String id;
  String name;
  String description;

  ProjectCategory({
    this.self,
    this.id,
    this.name,
    this.description,
  });

  ProjectCategory.fromJson(Map<String, dynamic> json) {
    self = json['self'];
    id = json['id'];
    name = json['name'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['self'] = this.self;
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    return data;
  }
}
