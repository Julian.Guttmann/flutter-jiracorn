class ServerInfo {
  String baseUrl;
  String version;
  List<int> versionNumbers;
  String deploymentType;
  int buildNumber;
  String buildDate;
  String serverTime;
  String scmInfo;
  String serverTitle;

  ServerInfo({
    this.baseUrl,
    this.version,
    this.versionNumbers,
    this.deploymentType,
    this.buildNumber,
    this.buildDate,
    this.serverTime,
    this.scmInfo,
    this.serverTitle,
  });

  ServerInfo.fromJson(Map<String, dynamic> json) {
    baseUrl = json['baseUrl'];
    version = json['version'];
    versionNumbers = json['versionNumbers'].cast<int>();
    deploymentType = json['deploymentType'];
    buildNumber = json['buildNumber'];
    buildDate = json['buildDate'];
    serverTime = json['serverTime'];
    scmInfo = json['scmInfo'];
    serverTitle = json['serverTitle'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['baseUrl'] = this.baseUrl;
    data['version'] = this.version;
    data['versionNumbers'] = this.versionNumbers;
    data['deploymentType'] = this.deploymentType;
    data['buildNumber'] = this.buildNumber;
    data['buildDate'] = this.buildDate;
    data['serverTime'] = this.serverTime;
    data['scmInfo'] = this.scmInfo;
    data['serverTitle'] = this.serverTitle;
    return data;
  }
}
