import 'package:jiracorn/src/bloc/utils/bloc_base.dart';
import 'package:jiracorn/src/resource/provider/authentication_api_provider.dart';
import 'package:rxdart/rxdart.dart';

class AuthenticationBloc extends BlocBase {
  final _authenticationApiProvider = AuthenticationApiProvider();

  final _username = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();
  final _jiraUrl = BehaviorSubject<String>();

  Stream<bool> get submitValid =>
      Observable.combineLatest3(username, password, jiraUrl, (e, p, q) => true);

  // retrieve data from stream
  Stream<String> get username => _username.stream;
  Stream<String> get password => _password.stream;
  Stream<String> get jiraUrl => _jiraUrl.stream;

  // add data to stream
  Function(String) get setUsername => _username.sink.add;
  Function(String) get setPassword => _password.sink.add;
  Function(String) get setJiraUrl => _jiraUrl.sink.add;

  Future<bool> submit() async {
    final validUsername = _username.value;
    final validPassword = _password.value;
    final validJiraUrl = _jiraUrl.value;
    return _authenticationApiProvider.authenticate(
        validJiraUrl, validUsername, validPassword);
  }

  @override
  dispose() {
    _username.close();
    _password.close();
    _jiraUrl.close();
  }
}
