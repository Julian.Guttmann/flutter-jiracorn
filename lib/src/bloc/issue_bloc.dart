import 'dart:async';

import 'package:jiracorn/src/bloc/utils/bloc_base.dart';
import 'package:jiracorn/src/model/issue/all_issue.dart';
import 'package:jiracorn/src/model/issue/issue.dart';
import 'package:jiracorn/src/resource/provider/issue_api_provider.dart';

class IssueBloc extends BlocBase {
  final IssueApiProvider issueApiProvider = IssueApiProvider();

  List<Issue> allLastViewedIssueList = [];
  List<Issue> allIssueByProjectIdList = [];

  int _lastViewedIssueIndex = 0;
  int _issueByProjectIdIndex = 0;

  int _lastViewedIssueTotal = 0;
  int _issueByProjectIdIndexTotal = 0;

  final _allLastViewedIssue = StreamController<List<Issue>>();
  Stream<List<Issue>> get allLastViewedIssue => _allLastViewedIssue.stream;

  final _allIssueByProjectId = StreamController<List<Issue>>();
  Stream<List<Issue>> get allIssueByProjectId => _allIssueByProjectId.stream;

  fetchAllLastViewedIssue() async {
    _issueByProjectIdIndex = 0;
    _issueByProjectIdIndexTotal = 0;

    if ((_lastViewedIssueIndex > _lastViewedIssueTotal)) {
      return;
    }

    AllIssue allLastViewedIssue =
        await issueApiProvider.fetchAllLastViewedIssue(_lastViewedIssueIndex);

    _lastViewedIssueIndex += allLastViewedIssue.maxResults;
    _lastViewedIssueTotal = allLastViewedIssue.total;

    allLastViewedIssueList.addAll(allLastViewedIssue.issues);
    _allLastViewedIssue.sink.add(allLastViewedIssueList);
  }

  fetchAllIssueByProjectId(String projectId) async {
    _lastViewedIssueIndex = 0;
    _lastViewedIssueTotal = 0;

    if ((_issueByProjectIdIndex > _issueByProjectIdIndexTotal)) {
      return;
    }
    AllIssue allIssueByProjectId = await issueApiProvider
        .fetchAllIssueByProjectID(projectId, _issueByProjectIdIndex);

    _issueByProjectIdIndex += allIssueByProjectId.maxResults;
    _issueByProjectIdIndexTotal = allIssueByProjectId.total;

    allIssueByProjectIdList.addAll(allIssueByProjectId.issues);
    _allIssueByProjectId.sink.add(allIssueByProjectIdList);
  }

  searchIssueInProject(String searchText, String projectId) async {
    if (searchText.isEmpty || projectId.isEmpty) {
      _allIssueByProjectId.sink.add(allIssueByProjectIdList);
      return;
    }

    AllIssue allSearchIssueInProject = await issueApiProvider
        .fetchAllSearchIssueInProject(searchText, projectId);

    _allIssueByProjectId.sink.add(allSearchIssueInProject.issues);
  }

  searchIssue(String searchText) async {
    if (searchText.isEmpty) {
      fetchAllLastViewedIssue();
      return;
    }
    AllIssue allSearchIssue =
        await issueApiProvider.fetchAllSeachIssue(searchText);

    _allLastViewedIssue.sink.add(allSearchIssue.issues);
  }

  @override
  void dispose() {
    _allLastViewedIssue.close();
    _allIssueByProjectId.close();

    allLastViewedIssueList.clear();
    allIssueByProjectIdList.clear();
  }
}
