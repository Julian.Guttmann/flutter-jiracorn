import 'dart:async';

import 'package:jiracorn/src/bloc/utils/bloc_base.dart';
import 'package:jiracorn/src/model/issue/issue.dart';
import 'package:jiracorn/src/resource/provider/issue_api_provider.dart';

class IssueDetailBloc extends BlocBase {
  final IssueApiProvider issueApiProvider = IssueApiProvider();

  final _currentIssue = StreamController<Issue>();
  Stream<Issue> get currentIssue => _currentIssue.stream;

  fetchCurrentIssue(String issueKey) async {
    if (issueKey.isEmpty) {
      return;
    }

    Issue currentIssue = await issueApiProvider.fetchIssueByKey(issueKey);
    _currentIssue.sink.add(currentIssue);
  }

  @override
  void dispose() {
    _currentIssue.close();
  }
}
