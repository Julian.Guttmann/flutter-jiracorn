import 'dart:async';

import 'package:jiracorn/src/bloc/utils/bloc_base.dart';
import 'package:jiracorn/src/model/server/server_info.dart';
import 'package:jiracorn/src/resource/provider/server_info_api_provider.dart';

class ServerInfoBloc extends BlocBase {
  final serverInfoApiProvider = ServerInfoApiProvider();

  final _serverInfo = StreamController<ServerInfo>.broadcast();

  Stream<ServerInfo> get serverInfo => _serverInfo.stream;

  fetchServerInfo() async {
    ServerInfo serverInfo = await serverInfoApiProvider.fetchServerInfo();
    _serverInfo.sink.add(serverInfo);
  }

  @override
  void dispose() {
    _serverInfo.close();
  }
}
