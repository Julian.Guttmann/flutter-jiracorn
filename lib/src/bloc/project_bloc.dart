import 'dart:async';

import 'package:jiracorn/src/bloc/utils/bloc_base.dart';
import 'package:jiracorn/src/model/project/project.dart';
import 'package:jiracorn/src/resource/provider/project_api_provider.dart';

class ProjectBloc extends BlocBase {
  final projectApiProvider = ProjectApiProvider();
  final _allProjectListItemFetcher =
      StreamController<List<Project>>.broadcast();

  Stream<List<Project>> get allProjectListItem =>
      _allProjectListItemFetcher.stream;

  fetchAllProjectListItem() async {
    List<Project> projectListItemModel =
        await projectApiProvider.fetchAllProjectListItem();
    _allProjectListItemFetcher.sink.add(projectListItemModel);
  }

  @override
  dispose() {
    _allProjectListItemFetcher.close();
  }
}
