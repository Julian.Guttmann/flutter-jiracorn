import 'dart:async';

import 'package:jiracorn/src/bloc/utils/bloc_base.dart';
import 'package:jiracorn/src/model/user/user.dart';
import 'package:jiracorn/src/resource/provider/profile_api_provider.dart';

class ProfileBloc extends BlocBase {
  final profileApiProvider = ProfileApiProvider();

  final _user = StreamController<User>.broadcast();

  Stream<User> get user => _user.stream;

  fetchUser() async {
    User user = await profileApiProvider.fetchUser();
    _user.sink.add(user);
  }

  @override
  void dispose() {
    _user.close();
  }
}
