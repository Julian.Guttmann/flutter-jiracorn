import 'package:jiracorn/src/bloc/utils/bloc_base.dart';
import 'package:jiracorn/src/model/issue/issue.dart';
import 'package:jiracorn/src/resource/provider/issue_api_provider.dart';

class IssueEditBloc extends BlocBase {
  final IssueApiProvider issueApiProvider = IssueApiProvider();

  Future<bool> updateIssue(Issue issue) async {
    return await issueApiProvider.updateIssue(issue);
  }

  @override
  void dispose() {}
}
