import 'package:flutter/material.dart';
import 'package:jiracorn/src/model/issue/issue.dart';
import 'package:jiracorn/src/widgets/issue/issue_list_item_widget.dart';

class IssueList extends StatefulWidget {
  final List<Issue> _items;
  final Function _fetchAllListItem;

  const IssueList(this._items, this._fetchAllListItem);

  @override
  _IssueListState createState() => _IssueListState();
}

class _IssueListState extends State<IssueList> {
  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void _scrollListener() {
    if (_controller.position.pixels == _controller.position.maxScrollExtent) {
      widget._fetchAllListItem();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: ListView.builder(
        controller: _controller,
        itemCount: widget._items.length,
        itemBuilder: (context, index) =>
            IssueListItem(issue: widget._items[index]),
      ),
    );
  }
}
