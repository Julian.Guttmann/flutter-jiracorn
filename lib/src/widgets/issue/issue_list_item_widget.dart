import 'package:flutter/material.dart';
import 'package:jiracorn/src/bloc/issue_detail_bloc.dart';
import 'package:jiracorn/src/bloc/utils/bloc_provider.dart';
import 'package:jiracorn/src/model/issue/issue.dart';
import 'package:jiracorn/src/ui/issue/issue_detail_page.dart';

class IssueListItem extends StatelessWidget {
  const IssueListItem({
    Key key,
    @required Issue issue,
  })  : _issue = issue,
        super(key: key);

  final Issue _issue;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return BlocProvider<IssueDetailBloc>(
                  bloc: IssueDetailBloc(),
                  child: IssueDetailPage(_issue.key),
                );
              },
            ),
          ),
          title: Text("${_issue.key}"),
          subtitle: Text("${_issue.fields.summary}"),
          trailing: Icon(Icons.keyboard_arrow_right),
        ),
        Divider(),
      ],
    );
  }
}
