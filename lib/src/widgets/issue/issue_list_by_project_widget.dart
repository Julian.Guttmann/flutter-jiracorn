import 'package:flutter/material.dart';
import 'package:jiracorn/src/model/issue/issue.dart';
import 'package:jiracorn/src/widgets/issue/issue_list_item_widget.dart';

class IssueListByProject extends StatefulWidget {
  final List<Issue> _items;
  final Function _fetchAllListItemByProject;
  final String _projectId;

  const IssueListByProject(
      this._items, this._fetchAllListItemByProject, this._projectId);

  @override
  _IssueListByProjectState createState() => _IssueListByProjectState();
}

class _IssueListByProjectState extends State<IssueListByProject> {
  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void _scrollListener() {
    if (_controller.position.pixels == _controller.position.maxScrollExtent) {
      widget._fetchAllListItemByProject(widget._projectId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: ListView.builder(
        controller: _controller,
        itemCount: widget._items.length,
        itemBuilder: (context, index) =>
            IssueListItem(issue: widget._items[index]),
      ),
    );
  }
}
